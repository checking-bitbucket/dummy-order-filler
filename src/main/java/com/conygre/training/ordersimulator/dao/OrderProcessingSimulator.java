package com.conygre.training.ordersimulator.dao;

import java.util.List;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;


/**
 * This class is a test harness class - it only exists for testing.
 * 
 * This is SIMULATING sending trade orders to an exchange for them to be filled.
 * 
 * This class uses the following codes to represent the state of an order:
 * 
 * 0 : initialized i.e. has not yet been processed at all
 * 1 : processing  i.e. the order has been sent to an exchange, we are waiting for a response
 * 2 : filled i.e. the order was successfully placed
 * 3 : rejected i.e. the order was not accepted by the trading exchange
 * 
 * The above are JUST SUGGESTED VALUES, you can change or improve as you see think is appropriate.
 */
@Service
public class OrderProcessingSimulator {
	
	// Standard mechanism for logging with spring boot - org.slf4j is built-in to spring boot
	private static final Logger LOG = LoggerFactory.getLogger(OrderProcessingSimulator.class);

	// You'll need to change these to match whatever you called your table and status_code field
	// You may also need to change the database name in application-mysql.properties and application-h2.properties
	// The default database name that's used here is "appDB"
	private static final String TABLE = "trade";
	private static final String STATUS_CODE = "statusCode";

	private int percentFailures = 10;

	@Autowired
	private TradeRepo tradeRepo;
	/**
	 * Any record in the configured database table with STATE=0 (init)
	 * 
	 * Will be changed to STATE=1 (processing)
	 */
	@Scheduled(fixedRateString = "${scheduleRateMs:10000}")
	public int findTradesForProcessing() {
//		String sql = "UPDATE " + TABLE + " SET " + STATUS_CODE + "=1 WHERE " + STATUS_CODE + "=0";
//		int numberChanged = template.update(sql);

//		List<Trade> trades = tradeRepo.findByStatusCode(0);
//		trades.forEach(trade -> trade.setStatusCode(1));
//		int numberChanged= trades.toArray().length;

//		tradeRepo.updateByStatusCode();
		
//		LOG.debug("Updated [" + numberChanged + "] order from initialized (0) TO processing (1)");
		List<Trade> trades = tradeRepo.findByStatusCode(0);
		if(trades.size()==0) return 1;
		for (Trade trade : trades) {
			trade.setStatusCode(1);
			tradeRepo.save(trade);
		}
		return 1;

	}
	
	/**
	 * Anything in the configured database table with STATE=1 (processing)
	 * 
	 * Will be changed to STATE=2 (filled) OR STATE=3 (rejected)
	 * 
	 * This method uses a random number to determine when trades are rejected.
	 */
	@Scheduled(fixedRateString = "${scheduleRateMs:15000}")
	public int findTradesForFillingOrRejecting() {
		int totalChanged = 0;
		int lastChanged = 0;

		do {
			lastChanged = 0;
			
			int randomInteger = new Random().nextInt(100);

			LOG.debug("Random number is [" + randomInteger +
					  "] , failure rate is [" + percentFailures + "]");
			
			// use a random number to decide if we'll simulate success OR failure
			if(randomInteger > percentFailures) {
				// Mark this one as success
				lastChanged = markTradeAsSuccessOrFailure(2);
				LOG.debug("Updated [" + lastChanged + "] order from processing (1) TO success (2)");
			}
			else {
				// Mark this one as failure!!
				lastChanged = markTradeAsSuccessOrFailure(3);
				LOG.debug("Updated [" + lastChanged + "] order from processing (1) TO failure (3)");
			}
			totalChanged += lastChanged;

		} while (lastChanged > 0);

		return totalChanged;
	}

	/*
	 * Update a single record to success or failure
	 */
	private int markTradeAsSuccessOrFailure(int successOrFailure) {
//		String sql = "UPDATE " + TABLE + " SET " + STATUS_CODE + "=" +
//	                 successOrFailure + " WHERE " + STATUS_CODE + "=1 limit 1";
//		return template.update(sql);

		Trade trade = tradeRepo.findFirstByStatusCode(1);
		if(trade==null) return 0;
		trade.setStatusCode(successOrFailure);
		tradeRepo.save(trade);
//		return trade.getId();

//		tradeRepo.updateOneByStatusCode(successOrFailure);

		return 1;


	}

	public void setPercentFailures(int percentFailures) {
		this.percentFailures = percentFailures;
	}

}