package com.conygre.training.ordersimulator.dao;
import javax.persistence.*;
@Table(name = "Trade")
@Entity
public class Trade {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "TradeID")
    private Integer id;


    private String timestamp = DateandTime.Dateandtime();




    public Trade(Integer id, String stockTicker, Integer volume, Double price, String buyOrSell, Integer statusCode) {
        this.id = id;
        this.stockTicker = stockTicker;
        this.volume = volume;
        this.price = price;
        this.buyOrSell = buyOrSell;
        this.statusCode = statusCode;

    }

    public Trade() {

    }

    private String stockTicker;
    private Integer volume;
    private Double price;
    private String buyOrSell;
    private Integer statusCode;

    public void setId(Integer id) {
        this.id = id;
    }

    public void setStockTicker(String stockTicker) {
        this.stockTicker = stockTicker;
    }

    public void setVolume(Integer volume) {
        this.volume = volume;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public void setBuyOrSell(String buyOrSell) {
        this.buyOrSell = buyOrSell;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public Integer getId() {
        return id;
    }

    public String getStockTicker() {
        return stockTicker;
    }

    public Integer getVolume() {
        return volume;
    }

    public Double getPrice() {
        return price;
    }

    public String getBuyOrSell() {
        return buyOrSell;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }




}