package com.conygre.training.ordersimulator.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TradeRepo extends JpaRepository<Trade,Integer> {
    List<Trade> findByStatusCode(Integer statusCode);

    Trade findFirstByStatusCode(Integer statusCode);

//    @Modifying
//    @Query("UPDATE Trade SET statusCode =1 WHERE statusCode=0")
//    void updateByStatusCode();
//
//    @Modifying
//    @Query(value = "UPDATE Trade SET statusCode =?1 WHERE statusCode=1 limit 1",nativeQuery = true)
//    void updateOneByStatusCode(Integer statusCode);


}
